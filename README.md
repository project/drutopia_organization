<!-- writeme -->
Drutopia Organization
=====================

Drutopia Organization provides an Organization content type for showing visitors information about organizations (groups, nonprofits, businesses) that are part of the site and can be associated with blogs, resources, events, etc.

 * https://gitlab.com/drutopia/drutopia_organization
 * Issues: https://gitlab.com/drutopia/drutopia_organization/issues
 * Source code: https://gitlab.com/drutopia/drutopia_organization/tree/8.x-1.x
 * Keywords: organization, business, group, nonprofit, content, drutopia
 * Package name: drupal/drutopia_organization


### Requirements

 * drupal/config_actions ^1.1
 * drupal/drutopia_core ^1.0
 * drupal/drutopia_seo ^1.0
 * drupal/field_group ^3.0
 * drupal/pathauto ^1.8
 * drupal/paragraphs ^1.12


### License

GPL-2.0+

<!-- endwriteme -->
